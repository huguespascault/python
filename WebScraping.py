from lxml import html
import requests



#================================================
# Define getHtml function

def getHtml(url):
    page = requests.get(url)
    tree = html.fromstring(page.content)
    return tree

#================================================ 
# Launch function

# html = getHtml('http://econpy.pythonanywhere.com/ex/001.html')


#================================================
# Define findTagValue function

def findTagValue(tree,tag, attributes):
    queryResult = tree.xpath('//' + tag + '[@' + attributes + ']/text()')
    return queryResult



#================================================ 
# Launch function

# buyersTagValue = findTagValue(html, 'div', 'title="buyer-name"')

