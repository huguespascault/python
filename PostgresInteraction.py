import psycopg2

#================================================
# Define insertInto function

def postgresConnect(HOST="", DATABASE="", USER="", PASSWORD=""):
    
    try:
        connection = psycopg2.connect(host = HOST,
                                      database = DATABASE, 
                                      user = USER, 
                                      password = PASSWORD)
        return connection
    
    except:
        print ("Connection failed")
    
    
    

#================================================
# Launch function

# connection = postgresConnect(HOST = "localhost",
#                              DATABASE = "database_collector",
#                              USER = "postgres",
#                              PASSWORD = "root")


# print(connection)

#================================================
#Create dict with row information

''' 
    row dict does not include auto-increment 
    variables like ID (SERIAL), some other rule
    need to be respect (cf:SQL Expression) in 
    terms of variables types(like DATE)
    
    [Example] :
    
    row = {
        "column": "'strValue'",
        "column": "intValue'",
        "column": "TO_DATE('xx/xx/xxxx', 'DD/MM/YYYY')",   
    }

'''

row = {
    "pageName": "'pythonae'",
    "Url": "'http://pythonan.com'",
    "RequestingDate": "TO_DATE('16/10/2018', 'DD/MM/YYYY')",
    "tagId" : "3"
    
}


#================================================
# Define insertInto function

def insertInto(table, row):
    
    cursor = connection.cursor()
    
    rowKeys = ""
    rowValues = ""
    count = 0
    
    #--------------------------------------------
    # First query formating
    
    for x in row:
        
        if count == 0:
            rowKeys = rowKeys + x
            rowValues = rowValues + row[x]
        elif count < len(row):
            rowKeys = rowKeys + ", " + x
            rowValues = rowValues + ", " + row[x]
        
        count = count + 1
    #--------------------------------------------    
    # Second query formating
    
    insert_query = "INSERT INTO page (" + rowKeys + ") VALUES (" + rowValues + ")"
    print (insert_query)
    
    #--------------------------------------------
    # Execute query
    cursor.execute(insert_query)    
    connection.commit()
    
#================================================ 
# Launch function

# insertInto("page",row)

#================================================



#================================================
# Define readTable function

def readTable(table):
    
    cursor = connection.cursor()
    
    insert_query = "SELECT * from " + table     
    cursor.execute(insert_query)
    rows = cursor.fetchall()
    
    for row in rows:
        print ("   ", row)
        
#================================================
# Launch function

# readTable("page")




#================================================
# Close connection

# connection.close()